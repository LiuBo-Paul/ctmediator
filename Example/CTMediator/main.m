//
//  main.m
//  CTMediator
//
//  Created by liubo on 03/09/2021.
//  Copyright (c) 2021 liubo. All rights reserved.
//

@import UIKit;
#import "CTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTAppDelegate class]));
    }
}
