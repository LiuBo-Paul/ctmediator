//
//  CTAppDelegate.h
//  CTMediator
//
//  Created by liubo on 03/09/2021.
//  Copyright (c) 2021 liubo. All rights reserved.
//

@import UIKit;

@interface CTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
