# CTMediator

[![CI Status](https://img.shields.io/travis/liubo/CTMediator.svg?style=flat)](https://travis-ci.org/liubo/CTMediator)
[![Version](https://img.shields.io/cocoapods/v/CTMediator.svg?style=flat)](https://cocoapods.org/pods/CTMediator)
[![License](https://img.shields.io/cocoapods/l/CTMediator.svg?style=flat)](https://cocoapods.org/pods/CTMediator)
[![Platform](https://img.shields.io/cocoapods/p/CTMediator.svg?style=flat)](https://cocoapods.org/pods/CTMediator)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CTMediator is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CTMediator'
```

## Author

liubo, 13621905107@163.com

## License

CTMediator is available under the MIT license. See the LICENSE file for more info.
