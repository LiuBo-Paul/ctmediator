#
# Be sure to run `pod lib lint CTMediator.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CTMediator'
  s.version          = '0.1.2'
  s.summary          = 'self CTMediator.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  'https://LiuBo-Paul@bitbucket.org/LiuBo-Paul/ctmediator.git'
                       DESC

  s.homepage         = 'https://LiuBo-Paul@bitbucket.org/LiuBo-Paul/ctmediator'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'liubo' => '13621905107@163.com' }
  s.source           = { :git => 'https://LiuBo-Paul@bitbucket.org/LiuBo-Paul/ctmediator.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'CTMediator/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CTMediator' => ['CTMediator/Assets/*.png']
  # }

#   s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
